angular.module("mobile-angular-ui", [
    'mobile-angular-ui.pointer-events', // prevents actions on disabled elements
    'mobile-angular-ui.active-links', // add .active class to active links
    'mobile-angular-ui.fastclick', // provides touch events with fastclick
    'mobile-angular-ui.scrollable', // polyfills overflow:auto with overthrow
    'mobile-angular-ui.directives.toggle',
    'mobile-angular-ui.directives.overlay',
    'mobile-angular-ui.directives.forms',
    'mobile-angular-ui.directives.panels',
    'mobile-angular-ui.directives.capture',
    'mobile-angular-ui.directives.sidebars',
    'mobile-angular-ui.directives.navbars',
    'mobile-angular-ui.directives.carousel'
]);


var app = angular.module('cigarcargo', ['ngRoute', 'mobile-angular-ui'])
    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/home', {
                    templateUrl: 'tmpl/home.html',
                    controller: 'BaseController'
                })
                .when('/add', {
                    templateUrl: 'tmpl/add.html',
                    controller: 'UploadController'
                })
                .otherwise({
                    redirectTo: '/home'
                });
        }
    ]);


app.factory('CigarEndpoint', [

    function() {
        var CigarType = Parse.Object.extend("CigarType");
        var query = new Parse.Query(CigarType);
        var out = {};


        return function(collectionUpdateFun) {
            var result = query.find({
                success: function(results) {
                    var out = results.map(function(e) {
                        return {
                            id: e.id,
                            image: e.get('image'),
                            brand: e.get('Brand'),
                            line: e.get('Line'),
                            price: Number(e.get('price')) / 100,
                            qty: Number(e.get('quantity'))
                        }
                    });
                    collectionUpdateFun(out);
                },

                error: function(results) {}
            });
        }

        return result;
    }
]);

app.factory('CigarUpload', function() {
    return function(obj, callback) {
        var CigarType = Parse.Object.extend("CigarType");
        var cigar = new CigarType();

        var imageFile = new Parse.File('photo.png', obj.image);
        imageFile.save().then(function() {
            console.log('Image saved');
            console.log(obj);

            cigar.set('price', Number(obj.price));
            cigar.set('Brand', obj.brand);
            cigar.set('Line', obj.line);
            cigar.set('quantity', obj.quantity);
            cigar.set('image', imageFile);

            cigar.save(null, {
                success: function(gameScore) {
                    // Execute any logic that should take place after the object is saved.
                    alert('New object created with objectId: ' + gameScore.id);
                    callback();
                },
                error: function(gameScore, error) {
                    // Execute any logic that should take place if the save fails.
                    // error is a Parse.Error with an error code and description.
                    alert('Failed to create new object, with error code: ' + error.description);
                }
            })
        }, function(error) {
            alert('Error');
        });
    };
})


app.controller('BaseController',
    function($scope, CigarEndpoint) {
        $scope.appname = "Cigar Cargo";
        $scope.cigars = [];
        $scope.updateCigars = function(arr) {
            $scope.cigars = arr;
            var brands = new Object();
            for (var i = arr.length - 1; i >= 0; i--) {
                brands[arr[i].brand] = null;
            };

            $scope.existingBrangs = Object.keys(brands);

            $scope.$apply();
            console.log(arr);
        };

        $scope.$on('cigarcargo.refresh.cigars', function(e, s) {
            CigarEndpoint($scope.updateCigars);
        });

        CigarEndpoint($scope.updateCigars);
    }
);

app.controller('UploadController', function($rootScope, $scope, $window, CigarUpload) {
    $scope.canUpload = true;
    $scope.upload = function(argument) {
        $scope.canUpload = false;
        var el = document.getElementById('imageUpload');
        console.log(el)
        var data = {
            line: $scope.line,
            brand: $scope.brand,
            image: el.files[0],
            price: $scope.price,
            quantity: $scope.quantity
        };

        CigarUpload(data, function() {
            $scope.canUpload = true;
            $scope.$apply();
            $window.history.back();
            $rootScope.$broadcast('cigarcargo.refresh.cigars');
        });
    }
});

app.directive('gridItem', [

    function($compile) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'tmpl/directive-grid-item.html',
            link: function($scope, $el, iAttrs) {}
        };
    }
])